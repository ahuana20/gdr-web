import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DefaultPageComponent } from './default-page/default-page.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

import { SidenavService } from 'src/app/@data/services/sidenav.service';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'home',
        component: DefaultPageComponent,
        data: {
          title: 'Home',
        },
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: '**',
        data: {
          title: '404 No encontrado👻 - Servir Talento Perú',
        },
        component: NotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
  menu = [];

  constructor(private sidenavService: SidenavService) {}
}
